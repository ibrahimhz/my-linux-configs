
vim.cmd("set expandtab")
vim.cmd("set tabstop=3")
vim.cmd("set softtabstop=3")
vim.cmd("set shiftwidth=3")
vim.cmd("set number")
vim.cmd("set relativenumber")

-- vim.cmd("set guicursor=n-v-c-i:block")
vim.opt.swapfile = false

-- Navigate Vim panes
vim.keymap.set('n', '<c-k>', ':wincmd k<CR>')
vim.keymap.set('n', '<c-j>', ':wincmd j<CR>')
vim.keymap.set('n', '<c-h>', ':wincmd h<CR>')
vim.keymap.set('n', '<c-l>', ':wincmd l<CR>')

-- Function to run the current Java file in a new tmux pane
_G.run_java_in_tmux = function()
  local file_path = vim.fn.expand("%:p")  -- Get the current file's absolute path
  local file_name_without_ext = vim.fn.expand("%:t:r")  -- Get the file name without extension

-- Construct the tmux command, ensuring paths are properly quoted
   local tmux_command = string.format(
    "tmux split-window -h 'bash -c \"javac \\\"%s\\\" && java -cp %s %s; read -p \\\"Press enter to close...\\\"\"'",
    file_path,
    vim.fn.expand("%:p:h"), -- Include the directory containing the class file in the classpath
    file_name_without_ext
   )

  -- Execute the tmux command
  vim.fn.system(tmux_command)
end

-- Set up a keybinding to run the Java file in tmux
vim.api.nvim_set_keymap('n', '<leader>rj', ':lua _G.run_java_in_tmux()<CR>', { noremap = true, silent = true })
