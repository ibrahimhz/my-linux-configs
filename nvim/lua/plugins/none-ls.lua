return {
	"nvimtools/none-ls.nvim",
   "nvimtools/none-ls-extras.nvim",
	config = function()
		local null_ls = require("null-ls")
		null_ls.setup({
			sources = {
				null_ls.builtins.formatting.stylua,
            null_ls.builtins.diagnostics.eslint_d,
            null_ls.builtins.diagnostics.prettier,
            null_ls.builtins.diagnostics.checkstyle,
            null_ls.builtins.diagnostics.google_java_format,
            null_ls.builtins.diagnostics.black,
            null_ls.builtins.diagnostics.isort,
			},
		})
		vim.keymap.set("n", "<leader>gf", vim.lsp.buf.format, {})
	end,
}
