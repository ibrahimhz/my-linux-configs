return {
   "catppuccin/nvim",
   name = "catppuccin",
   priority = 999,
   config = function()
      vim.cmd.colorscheme "catppuccin"
      -- Configure the catppuccin theme
      require("catppuccin").setup {
         -- Override the default highlight groups
         highlight_overrides = {
            mocha = function(mocha)
               return {
                  Comment = { fg = mocha.flamingo },
               }
            end,
         }
      }
      -- Directly set highlight groups for line numbers
      vim.api.nvim_set_hl(0, "LineNr", { fg = "white" })
      vim.api.nvim_set_hl(0, "CursorLineNr", { fg = "yellow" })
   end
}
